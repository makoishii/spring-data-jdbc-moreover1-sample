/* Drop Tables */
DROP TABLE IF EXISTS salary;
DROP TABLE IF EXISTS employee_projects; /* for spring-data-jpa-moreover */
DROP TABLE IF EXISTS project;           /* for spring-data-jpa-moreover */
DROP TABLE IF EXISTS contact_address;
DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS department;


/* Create Tables */
CREATE TABLE department
(
    id INTEGER PRIMARY KEY,
    name VARCHAR(128)
);

CREATE TABLE employee
(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128),
    joined_date DATE,
    department_id INTEGER,
    email VARCHAR(256),
    birth_day DATE,
    CONSTRAINT fk_department_id FOREIGN KEY (department_id) REFERENCES department(id)
);

CREATE TABLE contact_address
(
    employee_id INTEGER PRIMARY KEY,
    zip_code VARCHAR(8),
    address VARCHAR(512),
    phone_number VARCHAR(20),
    CONSTRAINT fk_employee_id FOREIGN KEY (employee_id) REFERENCES employee(id)
);

CREATE TABLE salary
(
    id             INTEGER PRIMARY KEY AUTO_INCREMENT,
    employee_id    INTEGER,
    year_and_month VARCHAR(8),
    amount         INTEGER,
    UNIQUE (employee_id, year_and_month),
    CONSTRAINT fk_salary_employee_id FOREIGN KEY (employee_id) REFERENCES employee (id)
);
