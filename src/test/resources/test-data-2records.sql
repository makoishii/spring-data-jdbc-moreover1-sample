INSERT INTO department VALUES(10, '営業部');
INSERT INTO department VALUES(20, '開発部');
INSERT INTO department VALUES(30, '管理部');
INSERT INTO department VALUES(31, '管理2部');
INSERT INTO department VALUES(32, '管理3部');

INSERT INTO employee VALUES(101, '山田太郎', '2010-04-01', 10, 'yamada@example.com', '1986-05-11');
INSERT INTO employee VALUES(102, '鈴木次郎', '2010-05-01', 20, 'suzuki@example.com', '1989-06-19');

INSERT INTO contact_address VALUES (101, '000-0000', 'Tokyo', '000-0000-0000');
INSERT INTO contact_address VALUES (102, '111-1111', 'Tokyo', '111-1111-1111');

INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (1, 101, '2023-01', 100);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (2, 101, '2023-02', 101);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (3, 101, '2023-03', 102);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (4, 101, '2023-04', 103);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (5, 101, '2023-05', 104);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (6, 101, '2023-06', 105);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (7, 101, '2023-07', 106);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (8, 101, '2023-08', 107);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (9, 101, '2023-09', 108);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (10, 101, '2023-10', 109);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (11, 101, '2023-11', 110);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (12, 101, '2023-12', 111);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (13, 102, '2023-01', 200);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (14, 102, '2023-02', 201);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (15, 102, '2023-03', 202);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (16, 102, '2023-04', 203);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (17, 102, '2023-05', 204);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (18, 102, '2023-06', 205);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (19, 102, '2023-07', 206);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (20, 102, '2023-08', 207);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (21, 102, '2023-09', 208);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (22, 102, '2023-10', 209);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (23, 102, '2023-11', 210);
INSERT INTO salary (id, employee_id, year_and_month, amount) VALUES (24, 102, '2023-12', 211);
