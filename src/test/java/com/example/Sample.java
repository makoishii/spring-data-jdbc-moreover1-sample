package com.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.data.relational.core.sql.In;

import java.util.*;

public class Sample {
    @Test
    void test1() {
        Map<Integer, List<String>> map = new HashMap<>();
        for(int i = 0; i < 1000000; ++ i) {
            List<String> list = map.get(i%100);
            if(list == null) {
                list = new ArrayList<>();
                map.put(i%100, list);
            }
            list.add(String.valueOf(i));
        }
    }

    @Test
    void test2() {
        Map<Integer, List<String>> map = new HashMap<>();
        for(int i = 0; i < 1000000; ++ i) {
            List<String> list = map.computeIfAbsent(i % 100, k -> new ArrayList<>());
            list.add(String.valueOf(i));
        }
    }

}
