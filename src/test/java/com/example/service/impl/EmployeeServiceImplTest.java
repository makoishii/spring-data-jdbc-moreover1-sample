package com.example.service.impl;

import com.example.persistence.entity.ContactAddress;
import com.example.persistence.entity.Employee;
import com.example.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EmployeeServiceImplTest {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeServiceImplTest(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

//    @BeforeEach
//    void setUp() throws Exception{
//        System.out.println("次の準備");
//        Thread.sleep(100);
//    }

    @Test
    @Sql({"/test-schema.sql", "/test-data-20records.sql"})
    void findEmployeeDtoAllByを計測() {
        employeeService.findEmployeeDtoAllBy().forEach(System.out::println);
        for (int i = 0; i < 1000; ++i) {
            employeeService.findEmployeeDtoAllBy();
        }
    }

    @Test
    @Sql({"/test-schema.sql", "/test-data-20records.sql"})
    void findEmployeeDtoAllBy2を計測() {
        employeeService.findEmployeeDtoAllBy2().forEach(System.out::println);
        for (int i = 0; i < 1000; ++i) {
            employeeService.findEmployeeDtoAllBy2();
        }
    }

    @Test
    @Sql({"/test-schema.sql", "/test-data-20records.sql"})
    void findEmployeeDtoAllByJoinQueryを計測() {
        employeeService.findEmployeeDtoAllByJoinQuery().forEach(System.out::println);
        for (int i = 0; i < 1000; ++i) {
            employeeService.findEmployeeDtoAllByJoinQuery();
        }
    }

    @Test
    @Sql({"/test-schema.sql", "/test-data-20records.sql"})
    void findAllを計測() {
        employeeService.findAll().forEach(System.out::println);
        for (int i = 0; i < 1000; ++i) {
            employeeService.findAll();
        }
    }

    @Test
    @Sql({"/test-schema.sql", "/test-data-20records.sql"})
    void findAllRowMapperを計測() {
        employeeService.findAllRowMapper().forEach(System.out::println);
        for (int i = 0; i < 1000; ++i) {
            employeeService.findAllRowMapper();
        }
    }

    @Test
    @Sql({"/test-schema.sql", "/test-data-20records.sql"})
    void findEmployeeContactAddressNullByを計測() {
        employeeService.findEmployeeContactAddressNullBy().forEach(System.out::println);
        for (int i = 0; i < 1000; ++i) {
            employeeService.findEmployeeContactAddressNullBy();
        }
    }

    @Test
    @Sql({"/test-schema.sql", "/test-data-20records.sql"})
    void findEmployeeContactAddressNullRowMapperByを計測() {
        employeeService.findEmployeeContactAddressNullRowMapperBy().forEach(System.out::println);
        for (int i = 0; i < 1000; ++i) {
            employeeService.findEmployeeContactAddressNullRowMapperBy();
        }
    }

    @Test
    @Sql({"/test-schema.sql", "/test-data-2records.sql"})
    void insert正常系() {
        Employee newEmployee = Employee.of("高田純平", LocalDate.of(2010, 04, 01),
                "takada@example.com", LocalDate.of(1979, 12, 01), 30,
                new ContactAddress("222-2222", "Tokyo", "222-2222-2222"));
        newEmployee = employeeService.insert(newEmployee);

        // 付与されたidが期待値通りか
        assertEquals(103, newEmployee.id());

        // 全件検索して、登録できているかを検証する
        // 期待値リストの作成 (test-data-2records.sqlの2件 + 追加したnewEmployee)
        List<Employee> expectedList = List.of(
                new Employee(101, "山田太郎", LocalDate.of(2010, 4, 1),
                        "yamada@example.com", LocalDate.of(1986, 5, 11), 10,
                        new ContactAddress("000-0000", "Tokyo", "000-0000-0000")),
                new Employee(102, "鈴木次郎", LocalDate.of(2010, 5, 1),
                        "suzuki@example.com", LocalDate.of(1989, 6, 19), 20,
                        new ContactAddress("111-1111", "Tokyo", "111-1111-1111")),
                newEmployee  // 追加したnewEmployee
        );
        // データベース上の実際値を取得
        List<Employee> actualList = employeeService.findAll();
        // 結果の検証 期待値と実際値が等しいか
        assertEquals(expectedList, actualList);
    }

    @Test
    @Sql({"/test-schema.sql", "/test-data-2records.sql"})
    void update正常系() {
        // 山田太郎さん(id:101)の社員情報を以下に変更する
        Employee updateEmployee = new Employee(101, "山田太郎", LocalDate.of(2010, 06, 01),
                "t.yamada@example.com", LocalDate.of(1979, 12, 01), 30,
                new ContactAddress("999-9999", "Okinawa", "999-9999-9999"));
        employeeService.update(updateEmployee);

        // 全件検索して、登録できているかを検証する
        // 期待値リストの作成 (更新したupdateEmployee + test-data-2records.sqlの2件のうち、更新していない方のもう1件)
        List<Employee> expectedList = List.of(
                updateEmployee,     // 更新した社員情報
                new Employee(102, "鈴木次郎", LocalDate.of(2010, 5, 1),
                        "suzuki@example.com", LocalDate.of(1989, 6, 19), 20,
                        new ContactAddress("111-1111", "Tokyo", "111-1111-1111"))
        );
        // データベース上の実際値を取得
        List<Employee> actualList = employeeService.findAll();
        // 結果の検証 期待値と実際値が等しいか
        assertEquals(expectedList, actualList);
    }

    @Test
    @Sql({"/test-schema.sql", "/test-data-2records.sql"})
    void deleteById_存在するID_レコード2件(){
        // テスト対象の呼び出し
        employeeService.delete(101);

        // 全件検索して、削除できているかを検証する
        // 期待値リストの作成
        List<Employee> expectedList = List.of(
                new Employee(102, "鈴木次郎", LocalDate.of(2010, 5, 1),
                        "suzuki@example.com", LocalDate.of(1989, 6, 19), 20,
                        new ContactAddress("111-1111", "Tokyo", "111-1111-1111"))
        );
        // データベース上の実際値を取得
        List<Employee> actualList = employeeService.findAll();
        // 結果の検証 期待値と実際値が等しいか
        assertEquals(expectedList, actualList);
    }

}