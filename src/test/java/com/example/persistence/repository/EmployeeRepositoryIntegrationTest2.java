package com.example.persistence.repository;

import com.example.persistence.entity.ContactAddress;
import com.example.persistence.entity.Department;
import com.example.persistence.entity.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// テスト対象:EmployeeRepositoryの統合テスト

@SpringBootTest
// @SpringBootTestを利用すると、@SpringBootApplicationが付いたクラスを見つけ出し、
// コンポーネントスキャン対象のBeanをテスト対象として使うことができます。
class EmployeeRepositoryIntegrationTest2 {
    // テスト対象(EmployeeRepository)をDIする
    // フィールド・インジェクションを利用
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private DepartmentRepository departmentRepository;

    // コンストラクタ・インジェクションを使うことも可能
    // テスト環境(src/test/java)で、コンストラクタ・インジェクションを使う場合、@Autowired は省略できない
//    @Autowired
//    public EmployeeRepositoryIntegrationTest(EmployeeRepository employeeRepository) {
//        this.employeeRepository = employeeRepository;
//    }

    @Test
    @Sql({"/test-schema.sql", "/test-data-20records.sql"})
    void test() {
        System.out.println("=== 新規社員登録 ===");
        Employee newEmployee =
                new Employee(null, "和田三郎", LocalDate.of(2020, 1, 1), "saburo@mail.com",
                        LocalDate.of(2000, 1, 1), 10,
                        new ContactAddress("100-000", "Japan", "999-9999-9999"));
        System.out.println("before save : newEmployee =" + newEmployee);
        System.out.println("=== employeeRepository.save(newEmployee) ===");
        Employee saveEmployee = employeeRepository.save(newEmployee);
        System.out.println("after save : newEmployee =" + newEmployee);
        System.out.println("saveEmployee =" + saveEmployee);
        System.out.println("newEmployee == saveEmployee ==>" + (newEmployee == saveEmployee));

        System.out.println("=== 部署id 10に所属する社員を検索 ===");
        System.out.println("=== employeeRepository.findByDepartmentId(10) ===");
        List<Employee> employees = employeeRepository.findByDepartmentId(10);
        for(Employee employee1 : employees){
            System.out.println(employee1);
        }

        System.out.println("=== 社員削除 (id=" + saveEmployee.id() + ") ===");
        System.out.println("===  employeeRepository.deleteById(" + saveEmployee.id() + ")===");
        employeeRepository.deleteById(saveEmployee.id());

        System.out.println("=== 社員削除後、もう一度 部署id 10に所属する社員を検索 ===");
        System.out.println("=== employeeRepository.findByDepartmentId(10) ===");
        List<Employee> employees2 = employeeRepository.findByDepartmentId(10);
        for(Employee employee1 : employees2){
            System.out.println(employee1);
        }

        System.out.println("=== departmentRepository.findAll() ===");
        List<Department> departments = departmentRepository.findAll();
        Set<Integer> departmentIds = new HashSet<>();
        for (Department department1 : departments){
            departmentIds.add(department1.id());
        }
        System.out.println("=== employeeRepository.findByDepartmentIdIn(departmentIds) ===");
        List<Employee> employees1 = employeeRepository.findByDepartmentIdIn(departmentIds);
        for (Employee employee :employees1){
            System.out.println(employee);
        }

        System.out.println("=== employeeRepository.findAll() ===");
        List<Employee> employees3 = employeeRepository.findAll();
        Set<Integer> departmentIds1 = new HashSet<>();
        for (Employee employee1 : employees3){
            departmentIds1.add(employee1.departmentId());
        }
        System.out.println("=== departmentRepository.findAllById(departmentIds1) ===");
        List<Department> departments1 = departmentRepository.findAllById(departmentIds1);
        for (Department department1 : departments1){
            System.out.println(department1);
        }
    }

}