package com.example.persistence.repository;

import com.example.persistence.entity.ContactAddress;
import com.example.persistence.entity.Employee;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

// テスト対象:EmployeeRepositoryの統合テスト

@SpringBootTest
// @SpringBootTestを利用すると、@SpringBootApplicationが付いたクラスを見つけ出し、
// コンポーネントスキャン対象のBeanをテスト対象として使うことができます。
class EmployeeRepositoryIntegrationTest {
    // テスト対象(EmployeeRepository)をDIする
    // フィールド・インジェクションを利用
    @Autowired
    private EmployeeRepository employeeRepository;

    // コンストラクタ・インジェクションを使うことも可能
    // テスト環境(src/test/java)で、コンストラクタ・インジェクションを使う場合、@Autowired は省略できない
//    @Autowired
//    public EmployeeRepositoryIntegrationTest(EmployeeRepository employeeRepository) {
//        this.employeeRepository = employeeRepository;
//    }

    @Nested
    class 検索系テスト {
        @Test
        @Sql({"/test-schema.sql", "/test-data-2records.sql"})
        void findAll_レコード2件() {
            // 期待値リストの作成
            List<Employee> expectedList = List.of(
                    new Employee(101, "山田太郎", LocalDate.of(2010, 4, 1),
                             "yamada@example.com", LocalDate.of(1986, 5, 11),10,
                            new ContactAddress("000-0000", "Tokyo", "000-0000-0000")),
                    new Employee(102, "鈴木次郎", LocalDate.of(2010, 5, 1),
                            "suzuki@example.com", LocalDate.of(1989, 6, 19), 20,
                            new ContactAddress("111-1111", "Tokyo", "111-1111-1111"))
            );
            // テスト対象を呼び出し、実際値のリストを取得
            List<Employee> actualList = employeeRepository.findAll(Sort.by("id").ascending());
            System.out.println("actualList: " + actualList);
            // 結果の検証 期待値と実際値が等しいか
            assertEquals(expectedList, actualList);
        }

        @Test
        @Sql({"/test-schema.sql"})
        void findAll_レコード0件() {
            // 期待値リストの作成
            List<Employee> expectedList = new ArrayList<>();
            // テスト対象を呼び出し、実際値のリストを取得
            List<Employee> actualList = employeeRepository.findAll(Sort.by("id").ascending());
            System.out.println("actualList: " + actualList);
            // 結果の検証 期待値と実際値が等しいか
            assertEquals(expectedList, actualList);
        }

        @Test
        @Sql({"/test-schema.sql", "/test-data-2records.sql"})
        void findById_存在するID_レコード2件() {
            // 期待値リストの作成
            Employee expectedEmployee =
                    new Employee(101, "山田太郎", LocalDate.of(2010, 4, 1),
                            "yamada@example.com", LocalDate.of(1986, 5, 11),10,
                            new ContactAddress("000-0000", "Tokyo", "000-0000-0000"));
            // テスト対象を呼び出し、実際値のリストを取得
            Optional<Employee> actualEmployeeOpt = employeeRepository.findById(101);
            // 結果の検証 期待値と実際値が等しいか
            assertEquals(expectedEmployee, actualEmployeeOpt.get());
        }

        @Test
        @Sql({"/test-schema.sql", "/test-data-2records.sql"})
        void findById_存在しないID_レコード2件() {
            // テスト対象を呼び出し、実際値のリストを取得
            Optional<Employee> actualEmployeeOpt = employeeRepository.findById(103);
            // 結果の検証 存在しなければ、期待通り
            assertTrue(actualEmployeeOpt.isEmpty());
        }
    }

    @Nested
    class 更新系テスト {
        @Test
        @Sql({"/test-schema.sql", "/test-data-2records.sql"})
        void save_レコード2件(){
            // 追加するEmployeeを生成
            Employee newEmployee = new Employee(null, "高田純平", LocalDate.of(2011, 4, 1),
                    "takada@example.com", LocalDate.of(1979, 12, 1), 30,
                    new ContactAddress("222-2222", "Tokyo", "222-2222-2222"));

            // テスト対象を呼び出す
            newEmployee = employeeRepository.save(newEmployee);
            System.out.println("newEmployee: " + newEmployee); // idには、自動採番された値が入っている

            // 全件検索して、登録できているかを検証する
            // 期待値リストの作成 (test-data-2records.sqlの2件 + 追加したnewEmployee)
            List<Employee> expectedList = List.of(
                    new Employee(101, "山田太郎", LocalDate.of(2010, 4, 1),
                            "yamada@example.com", LocalDate.of(1986, 5, 11),10,
                            new ContactAddress("000-0000", "Tokyo", "000-0000-0000")),
                    new Employee(102, "鈴木次郎", LocalDate.of(2010, 5, 1),
                            "suzuki@example.com", LocalDate.of(1989, 6, 19), 20,
                            new ContactAddress("111-1111", "Tokyo", "111-1111-1111")),
                    newEmployee  // 追加したnewEmployee
            );
            // データベース上の実際値を取得
            List<Employee> actualList = employeeRepository.findAll(Sort.by("id").ascending());
            // 結果の検証 期待値と実際値が等しいか
            assertEquals(expectedList, actualList);
        }

        @Test
        @Sql({"/test-schema.sql", "/test-data-2records.sql"})
        void deleteById_存在するID_レコード2件(){
            // テスト対象の呼び出し
            employeeRepository.deleteById(101);

            // 全件検索して、削除できているかを検証する
            // 期待値リストの作成
            List<Employee> expectedList = List.of(
                    new Employee(102, "鈴木次郎", LocalDate.of(2010, 5, 1),
                            "suzuki@example.com", LocalDate.of(1989, 6, 19), 20,
                            new ContactAddress("111-1111", "Tokyo", "111-1111-1111"))
            );
            // データベース上の実際値を取得
            List<Employee> actualList = employeeRepository.findAll(Sort.by("id").ascending());
            // 結果の検証 期待値と実際値が等しいか
            assertEquals(expectedList, actualList);
        }
    }
}