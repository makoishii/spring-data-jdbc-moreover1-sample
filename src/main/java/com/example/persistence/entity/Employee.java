package com.example.persistence.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.MappedCollection;

import java.time.LocalDate;

public record Employee(
        @Id
        Integer id,

        String name,

        LocalDate joinedDate,

        String email,

        LocalDate birthDay,

        Integer departmentId,

        @MappedCollection(idColumn = "employee_id")
        ContactAddress contactAddress) {

    // ミュータブルでも、wither (with+プロパティメソッド)を作っておけば、自動採番キー付与の際 Repository.save内で呼ぶ出される
    public Employee withId(Integer id) {
        return new Employee(id, name, joinedDate, email, birthDay, departmentId, contactAddress);
    }

    // アプリ側でcreateする際に利用するFactoryメソッド
    public static Employee of(String name, LocalDate joinedDate, String email, LocalDate birthDay, Integer departmentId, ContactAddress contactAddress){
        return new Employee(null, name, joinedDate, email, birthDay, departmentId, contactAddress);
    }

    public String toPretty() {
        String str1 =  "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", joinedDate=" + joinedDate +
                ", email='" + email + '\'' +
                ", birthDay=" + birthDay +
                ", departmentId=" + departmentId +
                ",\n  contactAddress=" + contactAddress +
                ",\n";
        return str1;
    }

//    // EmployeeをHashを使うMapのキーにする場合、全てのフィールドで計算させるとコストが高い
//    // 主キー(id)だけで、hash値を計算する
//    @Override
//    public int hashCode(){
//        return id.hashCode();
//    }
//
//    // equalsメソッドは全てフィールドが等しいかでちゃんと調べる。テストで期待値以外を検知できなくなるため。
//    @Override
//    public boolean equals(Object o){
//        return switch (o){
//            case Employee employee -> Objects.equals(id, employee.id) && Objects.equals(name, employee.name) && Objects.equals(joinedDate, employee.joinedDate) && Objects.equals(email, employee.email) && Objects.equals(birthDay, employee.birthDay) && Objects.equals(departmentId, employee.departmentId) && Objects.equals(contactAddress, employee.contactAddress);
//            case null, default -> false;
//        };
//    }
}
