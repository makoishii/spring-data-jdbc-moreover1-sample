package com.example.persistence.entity;

public record ContactAddress(

        String zipCode,

        String address,

        String phoneNumber) {

}
