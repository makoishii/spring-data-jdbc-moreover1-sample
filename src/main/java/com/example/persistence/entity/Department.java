package com.example.persistence.entity;

import org.springframework.data.annotation.Id;
//import org.springframework.data.relational.core.mapping.MappedCollection;

//import java.util.Set;

public record Department(
        @Id
        Integer id,

        String name

// Department ⇒ Employee (1対多)は 集約にしない
//    ,
//    @MappedCollection(idColumn = "department_id")
//    Set<Employee> employees

) {
    // ミュータブルでも、wither (with+プロパティメソッド)を作っておけば、自動採番キー付与の際 Repository.save内で呼ぶ出される
    public Department withId(Integer id) {
        return new Department(id, name);
    }
}
