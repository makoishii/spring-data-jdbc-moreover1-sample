package com.example.persistence.repository;

import com.example.persistence.entity.Department;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.repository.ListPagingAndSortingRepository;

import java.util.Map;
import java.util.Set;

public interface DepartmentRepository extends
        ListCrudRepository<Department, Integer>, ListPagingAndSortingRepository<Department, Integer> {
    String SQL_SELECT_DEPARTMENT_WHERE_ID_IN = "SELECT id, name FROM Department d WHERE d.id IN (:ids)";

    @Query(value = SQL_SELECT_DEPARTMENT_WHERE_ID_IN,
            resultSetExtractorRef = "idDepartmentMapResultExtractor")
    Map<Integer, Department> findIdEntityMapByIdIn(Set<Integer> ids);
}
