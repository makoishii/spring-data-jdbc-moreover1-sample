package com.example.persistence.repository.mapper;

import com.example.persistence.entity.ContactAddress;
import com.example.persistence.entity.Employee;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class EmployeeRowMapper implements RowMapper<Employee> {
    //        1     2       3              4        5            6
    // SELECT e.id, e.name, e.joined_date, e.email, e.birth_day, e.department_id,
    // 7           8          9
    // c.zip_code, c.address, c.phone_number
    @Override
    public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Employee(rs.getInt(1), rs.getString(2),
                rs.getDate(3).toLocalDate(), rs.getString(4),
                rs.getDate(5).toLocalDate(), rs.getInt(6),
                new ContactAddress(rs.getString(7),
                        rs.getString(8),
                        rs.getString(9)));
    }
}
