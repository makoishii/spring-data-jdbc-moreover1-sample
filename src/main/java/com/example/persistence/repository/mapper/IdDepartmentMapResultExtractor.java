package com.example.persistence.repository.mapper;

import com.example.persistence.entity.Department;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Component
public class IdDepartmentMapResultExtractor implements ResultSetExtractor<Map<Integer, Department>> {

    @Override
    public Map<Integer, Department> extractData(ResultSet rs) throws SQLException, DataAccessException {
        Map<Integer, Department> resultMap = new HashMap<>();
        while (rs.next()){
            Integer id = rs.getInt(1);
            Department department = new Department(id, rs.getString(2));
            resultMap.put(id, department);
        }
        return resultMap;
    }
}
