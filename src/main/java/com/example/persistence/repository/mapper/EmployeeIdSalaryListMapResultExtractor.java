package com.example.persistence.repository.mapper;

import com.example.persistence.entity.Salary;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class EmployeeIdSalaryListMapResultExtractor implements ResultSetExtractor<Map<Integer, List<Salary>>> {

    @Override
    public Map<Integer, List<Salary>> extractData(ResultSet rs) throws SQLException, DataAccessException {
        Map<Integer, List<Salary>> resultMap = new HashMap<>();
        while (rs.next()) {
            Integer employee_id = rs.getInt(2);
            Salary salary = new Salary(rs.getInt(1), employee_id,
                    rs.getString(3), rs.getInt(4));
            List<Salary> list = resultMap.computeIfAbsent(employee_id, k -> new ArrayList<>());
            // resultMap.computeIfAbsentは、以下の処理と同等。
//            List<Salary> list = resultMap.get(employee_id);
//            if (list == null) {
//                list = new ArrayList<>();
//                resultMap.put(employee_id, list);
//            }

            list.add(salary);
            // このlistはresultMap内のlistの参照なので、mapへのputは不要
        }
        return resultMap;
    }
}
