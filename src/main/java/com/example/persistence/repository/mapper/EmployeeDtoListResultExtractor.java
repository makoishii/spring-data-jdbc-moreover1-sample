package com.example.persistence.repository.mapper;

import com.example.dto.EmployeeDto;
import com.example.persistence.entity.ContactAddress;
import com.example.persistence.entity.Department;
import com.example.persistence.entity.Salary;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

// ResultSetExtractorを自作すると、ResultSetから自前でORマッピングすることができる
@Component
public class EmployeeDtoListResultExtractor implements ResultSetExtractor<List<EmployeeDto>> {

    @Override
    public List<EmployeeDto> extractData(ResultSet rs) throws SQLException, DataAccessException {
        // 対多の関連をjoinしたResultSetには重複した値を含む行が取得される
        // 各テーブルの主キーに着目して、一度取り込んだデータは再取得しない制御を組み込む。
        Map<Integer, Department> departmentMap = new HashMap<>(); // key:部署ID、value:Department
        Map<Integer, List<Salary>> salaryMap = new HashMap<>();   // key:社員ID、value:List<Salary>
        Map<Integer, EmployeeDto> employeeDtoMap = new LinkedHashMap<>(); // key:社員ID、value:EmployeeDto
        // ResultSetのデータ行で、employeeIdが全て連続しているのであれば、employeeDtoMapは不要で
        // 最初からList<EmployeeDto>にemployeeテーブル分を詰めていけばよい。
        // ユニークでない列でソートしている場合、連続しないケースがあるかもしれない???ので、一旦employeeDtoMapで保持する
        Integer beforeEmployeeId = 0; // 一つ前のemployeeId
        while (rs.next()) {
            Integer employeeId = rs.getInt("id");
            Integer departmentId = rs.getInt("department_id");

            if(!employeeId.equals(beforeEmployeeId)){ // 等しい場合は既にemployeeDtoMapに入れた重複するデータ。何もしない
                EmployeeDto employeeDto = employeeDtoMap.get(employeeId);
                if (employeeDto == null) { // nullではない場合は、既にemployeeDtoMapに入れた重複データ。何もしない
                    employeeDto = new EmployeeDto(employeeId,
                            rs.getString("name"),
                            rs.getDate("joined_date").toLocalDate(),
                            rs.getString("email"),
                            rs.getDate("birth_day").toLocalDate(),
                            departmentId,
                            new ContactAddress(rs.getString("zip_code"),
                                    rs.getString("address"),
                                    rs.getString("phone_number")),
                            null, null);
                    // 作成したemploeeDtoをemployeeDtoMapに保持
                    employeeDtoMap.put(employeeId, employeeDto);
                }
            }

            Department department = departmentMap.get(departmentId);
            if (department == null) {
                // まだdepartmentMapに入れていないdepartmentIdのデータのみ、departmentを作成
                department = new Department(departmentId, rs.getString("department_name"));
                // 作成したdepartmentをdepartmentMapに保持
                departmentMap.put(departmentId, department);
            } // nullではない場合は、既にdepartmentMapに入れた重複データなので何もしない

            // 対多の関連はEmployee-Salaryだけなので、Salaryが重複する行はない。全てsalaryMapに入れる。
            // 対多の関連が他にもある場合は、Salaryも重複するので、Map<emoloyeeId, List<Salary>>に加え
            // 重複チェック(Mapに取り込んだかの管理)用に、別途HashSet<salaryId>も使う必要がある。
            List<Salary> salaries = salaryMap.computeIfAbsent(employeeId, k -> new ArrayList<>());
            salaries.add(new Salary(rs.getInt("salary_id"),
                    employeeId, rs.getString("year_and_month"),
                    rs.getInt("amount")));
//          Map.computeIfAbsent 以下と同等の処理
//            List<Salary> salaries = salaryMap.get(employeeId);
//            if (salaries == null) {
//                salaries = new ArrayList<>();
//                salaryMap.put(employeeId, salaries);
//            }
//            salaries.add(new Salary(rs.getInt("salary_id"),
//                    employeeId, rs.getString("year_and_month"),
//                    rs.getInt("amount")));
            // salariesはsalaryMap内にあるリストのため、Mapへのputは不要

            beforeEmployeeId = employeeId; // 現在のemployeeIdを一つ前のIdに入れる
        }

        List<EmployeeDto> list = new ArrayList<>();
        for (Map.Entry<Integer, EmployeeDto> entry : employeeDtoMap.entrySet()) {
            // employeeDtoMapからEmployeeDtoを取り出す。このEmployeeDtoにはまだList<Salary>とDepartmentは入っていない
            Integer employeeId = entry.getKey();
            EmployeeDto employeeDto = entry.getValue();
            // List<Salary>とDepartmentを入れたEmployeeDtoを作り、List<EmployeeDto>に追加する
            list.add(employeeDto.withSalariesAndDepartment(salaryMap.get(employeeId),
                    departmentMap.get(employeeDto.departmentId())
            ));
        }
        // 作成したList<EmployeeDto>を返す
        return list;
    }
}
