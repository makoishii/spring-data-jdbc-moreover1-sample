package com.example.persistence.repository.mapper;

import com.example.persistence.entity.Employee;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class EmployeeContactAddressNullRowMapper implements RowMapper<Employee> {
    @Override
    public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
        Integer id = rs.getInt(1);
        return new Employee(id, rs.getString(2),
                rs.getDate(3).toLocalDate(), rs.getString(4),
                rs.getDate(5).toLocalDate(), rs.getInt(6), null);
    }
}
