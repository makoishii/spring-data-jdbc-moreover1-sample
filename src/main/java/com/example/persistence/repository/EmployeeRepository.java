package com.example.persistence.repository;

import com.example.dto.EmployeeDto;
import com.example.persistence.entity.Employee;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.repository.ListPagingAndSortingRepository;

import java.util.List;
import java.util.Set;

public interface EmployeeRepository extends
        ListCrudRepository<Employee, Integer>, ListPagingAndSortingRepository<Employee, Integer> {

    List<Employee> findByDepartmentId(Integer departmentId);

    List<Employee> findByDepartmentIdIn(Set<Integer> departmentIds);

    // ContactAddressを含まないEmployee 全件検索
    String SQL_SELECT_EMPLOYEE_ONLY_ALL =
        """
        SELECT id, name, joined_date, email, birth_day, department_id
        FROM employee e
        """;
    @Query(SQL_SELECT_EMPLOYEE_ONLY_ALL + "ORDER BY id")
    List<Employee> findEmployeeContactAddressNullBy();

    // デフォルトのMapperを使うより、以下を使うとより高速に検索ができる
    // ResultExtractor : ResultSet全体の変換 ⇒ 戻り値がList<Employee>のメソッドのみに適用可能
    // RowMapper : ResultSetの1行の変換 ⇒ 戻り値がEmployee,List<Employee>,Set<Employee>などに適用可能
    // ResultExtractorを使った場合とRowMapperを使った場合、速度は変わらないので、
    // RowMapperが使える場合はRowMapperを使えばよい。

    // ContactAddressを含まないEmployee 全件検索
    // RowMapperを使った実装サンプル
    @Query(value = SQL_SELECT_EMPLOYEE_ONLY_ALL + "ORDER BY id",
            rowMapperRef = "employeeContactAddressNullRowMapper")
    List<Employee> findEmployeeContactAddressNullRowMapperBy();

    // 全ての関連エンティティが入っているList<EmployeeDto>を返す全件検索メソッド
    // ResultExtractorを使った実装サンプル
    String SQL_SELECT_EMPLOYEE_DTO_ALL_JOIN = """
        SELECT e.id, e.name, e.joined_date, e.email, e.birth_day, e.department_id,
        c.zip_code, c.address, c.phone_number,
        s.id AS salary_id, s.year_and_month, s.amount,
        d.name AS department_name
        FROM employee e
        LEFT OUTER JOIN contact_address c ON c.employee_id = e.id
        LEFT OUTER JOIN salary s ON s.employee_id = e.id
        JOIN department d ON d.id = e.department_id
        """;
    @Query(value = SQL_SELECT_EMPLOYEE_DTO_ALL_JOIN + "ORDER BY e.id",
            resultSetExtractorRef = "employeeDtoListResultExtractor")
    List<EmployeeDto> findEmployeeDtoBy();

    // 参考 : RowMapper実装版のfindAll()メソッド
    // RowMapperを使った実装サンプル
    String SQL_FIND_ALL = """
        SELECT e.id, e.name, e.joined_date, e.email, e.birth_day, e.department_id,
        c.zip_code, c.address, c.phone_number
        FROM employee e
        LEFT OUTER JOIN contact_address c ON c.employee_id = e.id
        """;
    @Query(value = SQL_FIND_ALL + "ORDER BY e.id",
            rowMapperRef = "employeeRowMapper")
    List<Employee> findALLRowMapper();

}
