package com.example.persistence.repository;

import com.example.persistence.entity.Salary;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.repository.ListPagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface SalaryRepository extends ListCrudRepository<Salary, Integer>,
        ListPagingAndSortingRepository<Salary, Integer> {
    List<Salary> findByEmployeeIdIn(Set<Integer> employeeIds);

    String SQL_SELECT_SALARY_EMPLOYEE_ID_IN =
    """
    SELECT id, employee_id, year_and_month, amount
    FROM Salary s WHERE s.employee_id IN (:employeeIds)
    """;
    @Query(value = SQL_SELECT_SALARY_EMPLOYEE_ID_IN,
            resultSetExtractorRef = "employeeIdSalaryListMapResultExtractor")
    Map<Integer, List<Salary>> findEmployeeIdSalaryListMapByEmployeeIdIn(Set<Integer> employeeIds);


    // Spring Data JDBC で メソッド名から派生クエリを導出する deleteBy は 現行 利用できない。
    // deleteBy は、クローズしてしていないバグとして残っているため、実行時エラーとなる。(2023/1/23現在 spring-boot-starter-data-jdbc 3.0.0)
    //   https://github.com/spring-projects/spring-data-relational/issues/771
    // 整数型を返すdeleteByメソッドは、現行では、以下のように、@Queryを使って、手動でクエリを定義すれば動作する。
    @Modifying
    @Query("DELETE FROM salary WHERE employee_id = :employeeId")
    int deleteByEmployeeId(Integer employeeId);
    // <参考> Spring Data JDBC でのdeleteBy について
    // Spring Data JDBC のリファレンスでは、「付録 C: リポジトリクエリキーワード」 に以下のように記載されている。
    //   ここにリストされている一部のキーワードは特定のストアでサポートされていない可能性があるため、サポートされているキーワードの正確なリストについては、ストア固有のドキュメントを参照してください。
    // Spring Data JDBC のリファレンス本体(つまり、ストア固有のドキュメント)には、
    // 利用できるサブジェクトキーワードに関する言及がない。
    // (付録 C この下ある「述語キーワードと修飾子」については、Spring Data JDBC のリファレンス本体で利用できるもの言及されている)
    // よって、Spring Data JDBC の<<仕様>>として、deleteByが使えるかどうかは、不明瞭である。

}
