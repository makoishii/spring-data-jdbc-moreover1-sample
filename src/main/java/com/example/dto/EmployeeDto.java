package com.example.dto;

import com.example.persistence.entity.ContactAddress;
import com.example.persistence.entity.Department;
import com.example.persistence.entity.Employee;
import com.example.persistence.entity.Salary;

import java.time.LocalDate;
import java.util.List;

// EmployeeDao 全ての関連エンティティを含むクラス
// 関連エンティティ ContactAddress,List<Salary>,Department
public record EmployeeDto(
        Integer id,

        String name,

        LocalDate joinedDate,

        String email,

        LocalDate birthDay,

        Integer departmentId,

        ContactAddress contactAddress,

        List<Salary> salaries,

        Department department
) {
    static public EmployeeDto by(Employee employee, List<Salary> salaries,
                                 Department department) {
        return new EmployeeDto(employee.id(), employee.name(), employee.joinedDate(),
                employee.email(), employee.birthDay(), employee.departmentId(),
                employee.contactAddress(),
                salaries, department);
    }
    public EmployeeDto withSalariesAndDepartment(List<Salary> salaries, Department department) {
        return new EmployeeDto(id, name, joinedDate, email, birthDay, departmentId, contactAddress,
                salaries, department);
    }

    public String toPretty() {
        String str1 =  "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", joinedDate=" + joinedDate +
                ", email='" + email + '\'' +
                ", birthDay=" + birthDay +
                ", departmentId=" + departmentId +
                ",\n  contactAddress=" + contactAddress +
                ",\n";
        StringBuilder str2Builder = new StringBuilder("  salarys=\n");
        for (Salary salary : salaries){
            str2Builder.append("    ").append(salary).append("\n");
        }
        str2Builder.append("}\n")
                .append(department); // Departmentも追加
        return str1 + str2Builder.toString();
    }

}
