package com.example.service.impl;

import com.example.dto.EmployeeDto;
import com.example.persistence.entity.Department;
import com.example.persistence.entity.Employee;
import com.example.persistence.entity.Salary;
import com.example.persistence.repository.DepartmentRepository;
import com.example.persistence.repository.EmployeeRepository;
import com.example.persistence.repository.SalaryRepository;
import com.example.service.EmployeeService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private static final Sort DEFAULT_SORT = Sort.by("id").ascending(); // id昇順
    private final EmployeeRepository employeeRepository;
    private final SalaryRepository salaryRepository;
    private final DepartmentRepository departmentRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, SalaryRepository salaryRepository, DepartmentRepository departmentRepository) {
        this.employeeRepository = employeeRepository;
        this.salaryRepository = salaryRepository;
        this.departmentRepository = departmentRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Employee> findAll() {
        return employeeRepository.findAll(DEFAULT_SORT);
    }

    // 参考 : RowMapper実装版のfindAll()メソッド
    @Override
    @Transactional(readOnly = true)
    public List<Employee> findAllRowMapper() {
        return employeeRepository.findALLRowMapper();
    }

    // サービスクラスにて各エンティティをN+1対応で取得しEmployeeDtoを構築
    // RepositoryのList<エンティティ>を返すSELECT ... WHERE IN (...)を利用
    @Override
    @Transactional(readOnly = true)
    public List<EmployeeDto> findEmployeeDtoAllBy() {
        // Employee全件検索
        List<Employee> employees = employeeRepository.findAll(DEFAULT_SORT);

        // List<Department>の取得前準備 : Set<部署id>を生成
        // List<Salary> の取得前準備 : Set<社員id>の生成
        Set<Integer> departmentIds = new LinkedHashSet<>();
        Set<Integer> employeeIds = new LinkedHashSet<>();
        for (Employee employee : employees){
            employeeIds.add(employee.id());
            departmentIds.add(employee.departmentId());
        }

        // Set<部署id>で指定したDepartmentを検索
        List<Department> departments = departmentRepository.findAllById(departmentIds);
        // Map<部署id, Department>を生成
        Map<Integer, Department> departmentMap =
                departments.stream().collect(Collectors.toMap(department -> department.id(), department -> department));

        // Set<社員id>で指定したSalaryを検索
        List<Salary> salaries = salaryRepository.findByEmployeeIdIn(employeeIds);
        // Map<部署id, Department>を生成
        Map<Integer, List<Salary>> salaryMap = new HashMap<>();
        for(Salary salary : salaries) {
            List<Salary> salaryList = salaryMap.get(salary.employeeId());
            if(salaryList == null){
                salaryList = new ArrayList<>();
                salaryMap.put(salary.employeeId(), salaryList);
            }
            salaryList.add(salary);
            // salaryListはMap内のListなので、Mapへのputは不要
        }

        // List<EmployeeDto>を作成
        List<EmployeeDto> list =
                employees.stream().map(employee -> EmployeeDto.by(employee,
                                salaryMap.get(employee.id()), departmentMap.get(employee.departmentId())))
                        .toList();
        return list;
    }

    // サービスクラスにて各エンティティをN+1対応で取得しEmployeeDtoを構築
    // RepositoryのMap<主キー, エンティティ>を返すSELECT ... WHERE IN (...)を利用
    @Override
    @Transactional(readOnly = true)
    public List<EmployeeDto> findEmployeeDtoAllBy2() {
        // Employee全件検索
        List<Employee> employees = employeeRepository.findAll(DEFAULT_SORT);

        // List<Department>の取得前準備 : Set<部署id>を生成
        // List<Salary> の取得前準備 : Set<社員id>の生成
        Set<Integer> departmentIds = new LinkedHashSet<>();
        Set<Integer> employeeIds = new LinkedHashSet<>();
        for (Employee employee : employees){
            employeeIds.add(employee.id());
            departmentIds.add(employee.departmentId());
        }

        // Set<部署id>で指定したDepartmentを検索
        // 戻り値 Map<部署id, Department>のメソッド呼び出し
        Map<Integer, Department> departmentMap = departmentRepository.findIdEntityMapByIdIn(departmentIds);

        // Set<社員id>で指定したSalaryを検索
        // 戻り値 Map<社員id, List<Salary>>のメソッド呼び出し
        Map<Integer, List<Salary>> salaryMap = salaryRepository.findEmployeeIdSalaryListMapByEmployeeIdIn(employeeIds);

        // List<EmployeeDto>を作成
        return employees.stream().map(employee -> EmployeeDto.by(employee,
                        salaryMap.get(employee.id()), departmentMap.get(employee.departmentId())))
                        .toList();
    }

    // @Queryによる全エンティティ取得 全件検索メソッド
    @Override
    @Transactional(readOnly = true)
    public List<EmployeeDto> findEmployeeDtoAllByJoinQuery() {
        return employeeRepository.findEmployeeDtoBy();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Employee> findEmployeeContactAddressNullBy() {
        return employeeRepository.findEmployeeContactAddressNullBy();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Employee> findEmployeeContactAddressNullRowMapperBy() {
        return employeeRepository.findEmployeeContactAddressNullRowMapperBy();
    }

    @Override
    @Transactional
    public Employee insert(Employee employee) {
        if(employee.id() != null){
            throw new IllegalArgumentException("社員IDを指定した登録はできません。");
        }
        Integer departmentId = employee.departmentId();
        if(departmentId == null || departmentId < 1){
            throw new IllegalArgumentException("所属部署IDが不正です。(departmentId=" + departmentId + ")");
        }
        if(!departmentRepository.existsById(departmentId)){
            throw new IllegalArgumentException("指定された所属部署IDの部署はありません。(departmentId=" + departmentId + ")");
        }
        return employeeRepository.save(employee);
    }

    @Override
    @Transactional
    public void delete(Integer employeeId) {
        if(employeeId == null || employeeId < 1){
            throw new IllegalArgumentException("社員IDが不正です。(employeeId=" + employeeId + ")");
        }
        if(!employeeRepository.existsById(employeeId)){
            throw new IllegalArgumentException("指定されたIDの社員は存在しません。(employeeId=" + employeeId + ")");
        }
        int deleteCount = salaryRepository.deleteByEmployeeId(employeeId);
        System.out.println("社員(id=" + employeeId + ")のSalaryを" + deleteCount + "件削除しました");
        employeeRepository.deleteById(employeeId);
    }

    @Override
    @Transactional
    public void update(Employee employee) {
        Integer employeeId = employee.id();
        if(employeeId == null || employeeId < 1){
            throw new IllegalArgumentException("社員IDが不正です。(employeeId=" + employeeId + ")");
        }
        if(!employeeRepository.existsById(employeeId)){
            throw new IllegalArgumentException("指定されたIDの社員は存在しません。(employeeId=" + employeeId + ")");
        }
        if(!departmentRepository.existsById(employee.departmentId())){
            throw new IllegalArgumentException("指定された所属部署IDの部署はありません。(departmentId=" + employee.departmentId() + ")");
        }
        // ContactAddressの省略は無い仕様とする (nullなら現行のまま変更なしの仕様にする場合はロジック修正)
        employeeRepository.save(employee);
    }

}
