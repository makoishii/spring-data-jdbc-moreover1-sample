package com.example.service;

import com.example.dto.EmployeeDto;
import com.example.persistence.entity.Employee;

import java.util.List;

public interface EmployeeService {
    // --- GraphQL @QueryMappingから利用するメソッド --------------------------------------
    // ContactAddressが不要な場合は無駄になるが、
    // 1対1関連であるContactAddressをjoinしたselectはさほど遅くならないため、
    // ContactAddressまで取得する findAllメソッドと
    // ContactAddressは取得しないfindEmployeeContactAddressNullByメソッドを用意。
    List<Employee> findAll();

    // 参考 findAll()と同等の検索機能
    // 高速処理版 RowMapper実装版
    List<Employee> findAllRowMapper();

    // findAllと異なり、ContactAddressは収集しない。contactAddressにはnullが設定され返る
    List<Employee> findEmployeeContactAddressNullBy();

    // findAllと異なり、ContactAddressは収集しない。contactAddressにはnullが設定され返る
    // 高速処理版 RowMapper利用
    List<Employee> findEmployeeContactAddressNullRowMapperBy();

    // --- GraphQL @QueryMappingから利用するメソッド ここまで ------------------------------

    // サービスクラスにて各エンティティをN+1対応で取得しEmployeeDtoを構築
    // RepositoryのList<エンティティ>を返すSELECT ... WHERE IN (...)を利用
    List<EmployeeDto> findEmployeeDtoAllBy();

    // サービスクラスにて各エンティティをN+1対応で取得しEmployeeDtoを構築
    // RepositoryのMap<主キー, エンティティ>を返すSELECT ... WHERE IN (...)を利用
    List<EmployeeDto> findEmployeeDtoAllBy2();

    // 参考 : @Queryによる全エンティティ取得 全件検索メソッド
    // 高速処理版 ResultSetExtractor実装版
    List<EmployeeDto> findEmployeeDtoAllByJoinQuery();

    Employee insert(Employee employee);

    void delete(Integer id);

    void update(Employee employee);
}
