package com.example;

import com.example.dto.EmployeeDto;
import com.example.persistence.entity.Department;
import com.example.persistence.entity.Employee;
import com.example.persistence.entity.Salary;
import com.example.persistence.repository.DepartmentRepository;
import com.example.persistence.repository.EmployeeRepository;
import com.example.persistence.repository.SalaryRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.*;

@SpringBootApplication
public class SpringDataJdbcMoreover1SampleApplication implements ApplicationRunner {

    private final DepartmentRepository departmentRepository;
    private final SalaryRepository salaryRepository;
    private final EmployeeRepository employeeRepository;

    public SpringDataJdbcMoreover1SampleApplication(DepartmentRepository departmentRepository, SalaryRepository salaryRepository, EmployeeRepository employeeRepository) {
        this.departmentRepository = departmentRepository;
        this.salaryRepository = salaryRepository;
        this.employeeRepository = employeeRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringDataJdbcMoreover1SampleApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // GraphQL方式のN+1問題解決
        // まず、ルートのエンティティをSELECT All する。
        System.out.println("=== employeeRepository.findAll() ===");
        List<Employee> employees = employeeRepository.findAll();

        // 関連エンティティの主キーをかき集める
        Set<Integer> departmentIds = new HashSet<>();
        Set<Integer> emproyeeIds = new HashSet<>();
        for (Employee employee : employees) {
            System.out.println(employee);
            departmentIds.add(employee.departmentId());
            emproyeeIds.add(employee.id());
        }

        // 関連エンティティ(Department)を SELECT WHERE IN (…) で取得
        System.out.println("=== departmentRepository.findAllById(departmentIds1) ===");
        List<Department> departments = departmentRepository.findAllById(departmentIds);
        for (Department department : departments) {
            System.out.println(department);
        }

        // 関連エンティティ(Salary)を SELECT WHERE IN (…) で取得
        System.out.println("=== salaryRepository.findByEmployeeIdIn(emproyeeIds) ===");
        List<Salary> salaries = salaryRepository.findByEmployeeIdIn(emproyeeIds);

        // 関連エンティティ(Department)取得の SELECT WHERE IN (…) を Map<id, Department>型で取得するResultSetExecutorを自作した場合
        System.out.println("=== departmentRepository.findIdEntityMapByIdIn(departmentIds) ===");
        Map<Integer, Department> idDepartmentMap = departmentRepository.findIdEntityMapByIdIn(departmentIds);
        System.out.println("Department.id ==> Department");
        for (Map.Entry<Integer, Department> entry: idDepartmentMap.entrySet()) {
            System.out.println("id=" + entry.getKey() + ", ==> " + entry.getValue());
        }

        // 関連エンティティ(List<Salary>)取得の SELECT WHERE IN (…) を Map<id, List<Salary>>型で取得するResultSetExecutorを自作した場合
        System.out.println("=== salaryRepository.findEmployeeIdSalaryListMapByEmployeeIdIn(emproyeeIds) ===");
        Map<Integer, List<Salary>> idSalaryMap = salaryRepository.findEmployeeIdSalaryListMapByEmployeeIdIn(emproyeeIds);
        System.out.println("Employee.id ==> List<Salary>");
        for (Map.Entry<Integer, List<Salary>> entry: idSalaryMap.entrySet()) {
            System.out.println("id=" + entry.getKey() + ", ==> " + entry.getValue());
        }

        List<EmployeeDto> list3 = method1(employees, departments, salaries);
    }

    List<EmployeeDto> method1(List<Employee> employees, List<Department> departments, List<Salary> salaries) {
        Map<Integer, Department> departmentMap = new HashMap<>();
        for (Department department : departments) {
            departmentMap.put(department.id(), department);
        }
        Map<Integer, List<Salary>> salaryMap = new HashMap<>();
        for (Salary salary : salaries) {
            List<Salary> salaryList = salaryMap.get(salary.employeeId());
            if (salaryList == null) {
                salaryList = new ArrayList<>();
                salaryList.add(salary);
                salaryMap.put(salary.employeeId(), salaryList);
            } else {
                salaryList.add(salary);
            }
        }

        List<EmployeeDto> list = new ArrayList<>();
        for (Employee employee : employees) {
            list.add(EmployeeDto.by(employee, salaryMap.get(employee.id()),
                    departmentMap.get(employee.departmentId())));
        }
        return list;
    }
}
